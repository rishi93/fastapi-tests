import nox


@nox.session
def lint(session):
    session.install("isort")
    session.install("black")
    session.run("isort", ".")
    session.run("black", ".")


@nox.session
def tests(session):
    # TODO: Add tests
    pass
