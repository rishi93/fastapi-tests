import tensorflow as tf
from fastapi import APIRouter, Request, UploadFile
from PIL import Image

router = APIRouter(prefix="/prediction")


def model_prediction(model, im):
    class_names = ["cat", "dog"]
    # im = tf.keras.utils.load_img(test_image, target_size=(160, 160))
    # im = tf.keras.utils.img_to_array(im)
    im = tf.expand_dims(im, axis=0)
    prediction = model.predict(im)
    prediction_label = class_names[round(tf.nn.sigmoid(prediction).numpy().item())]
    return prediction_label


@router.post("/")
async def get_prediction(request: Request, upload_file: UploadFile):
    im = Image.open(upload_file.file)
    im = im.resize((160, 160))

    prediction = model_prediction(request.app.state.my_model, im)
    return {"message": f"This is a {prediction.title()}!"}
