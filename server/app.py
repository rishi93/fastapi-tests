import os
from contextlib import asynccontextmanager

import tensorflow as tf
from fastapi import FastAPI

from server.routes.predictions import router as predictions_router
from server.routes.users import router as users_router


@asynccontextmanager
async def lifespan(app: FastAPI):
    print(f"Starting up the server")
    model_path = os.getcwd() + "/cats_dogs.h5"
    app.state.my_model = tf.keras.models.load_model(model_path, compile=False)
    yield
    print(f"Shutting down the server")


app = FastAPI(lifespan=lifespan)


@app.get("/")
def get_root():
    """A basic healthcheck route

    Returns:
        ResponseModel
    """
    return {"message": "Hello world!"}


app.include_router(users_router)
app.include_router(predictions_router)
